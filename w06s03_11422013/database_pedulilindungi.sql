-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2023 at 05:39 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_pedulilindungi`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `password`, `last_login`) VALUES
(1, 'cynthiadn', 'qweasd123', '2023-02-22 10:26:53'),
(2, 'apriatn', 'apriatn123', '2023-02-22 10:27:40'),
(3, 'Johannes', 'oan123', '0000-00-00 00:00:00'),
(4, 'Jeremia', 'jeremia123', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `check_in`
--

CREATE TABLE `check_in` (
  `id_checkIn` int(11) NOT NULL,
  `pengunjung_id` int(11) NOT NULL,
  `lokasi_id` int(11) NOT NULL,
  `datetime_checkIn` datetime NOT NULL,
  `datetime_checkOut` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `check_in`
--

INSERT INTO `check_in` (`id_checkIn`, `pengunjung_id`, `lokasi_id`, `datetime_checkIn`, `datetime_checkOut`) VALUES
(1, 3, 4, '2023-03-17 05:22:19', '2023-03-17 05:22:22'),
(2, 4, 5, '2023-03-17 05:27:56', '2023-03-17 05:27:57');

-- --------------------------------------------------------

--
-- Table structure for table `faskes`
--

CREATE TABLE `faskes` (
  `id_faskes` int(11) NOT NULL,
  `nama_faskes` varchar(120) NOT NULL,
  `alamat_faskes` varchar(120) NOT NULL,
  `kategori_faskes` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faskes`
--

INSERT INTO `faskes` (`id_faskes`, `nama_faskes`, `alamat_faskes`, `kategori_faskes`) VALUES
(1, '.Dinas Kesehatan Toba.', 'Dinas Kesehatan Toba', 'Dinas Kesehatan'),
(2, '.Klinik PT Inalum.', 'Ambar Halim, Pintu Pohan Merant, Toba Samosir', 'klinik'),
(3, '.Klinik PT Inalum.', 'Jl. Gereja No. 17, Lumban Dolok Haume Bange,Balige', 'Rumah Sakit'),
(4, '.RSUd. Porsea.', 'Jl. Raja Sipakko Napitupulu,Parparean I,Porsea', 'Rumah Sakit'),
(5, '.Klinik Polres Toba Samosir.', 'Jl. Porsea Kab Toba Samosir,Sumatera Utara,Indonesia', 'klinik'),
(6, '.Klinik Institut Teknologi Del.', 'Jl. P.I. Del, Sitoluama,Lagu Boti,Toba ,Sumatera Utara', 'klinik');

-- --------------------------------------------------------

--
-- Table structure for table `faskes_toba`
--

CREATE TABLE `faskes_toba` (
  `id_faskes` int(11) NOT NULL,
  `nama_faskes` varchar(100) NOT NULL,
  `alamat_faskes` varchar(100) NOT NULL,
  `kategori_faskes` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faskes_toba`
--

INSERT INTO `faskes_toba` (`id_faskes`, `nama_faskes`, `alamat_faskes`, `kategori_faskes`) VALUES
(1, 'Dinas Kesehatan Toba', 'Jl. Sutomo Pagar Batu No 1,Balige III', 'Dinas Kesehatan'),
(2, 'Klinik PT Inalum', 'Ambar Halim, Pintu Pohan Merant, Toba Samosir', 'Klinik'),
(3, 'RSU. HKBP Balige', 'Jl. Gereja No. 17, Lumban Dolok Haume Bange,Balige', 'Rumah Sakit'),
(4, 'RSUd. Porsea', 'Jl. Raja Sipakko Napitupulu,Parparean I,Porsea', 'Rumah Sakit'),
(5, 'Klinik Polres Toba Samosir', 'Jl. Porsea Kab Toba Samosir,Sumatera Utara,Indonesia', 'Klinik'),
(6, 'Klinik Institut Teknologi Del', 'Jl. P.I. Del, Sitoluama,Lagu Boti,Toba ,Sumatera Utara', 'Klinik');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_vaksin`
--

CREATE TABLE `kategori_vaksin` (
  `id_vaksin` int(11) NOT NULL,
  `nama_vaksin` varchar(100) NOT NULL,
  `jeda_vaksin` varchar(50) NOT NULL,
  `dosis` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori_vaksin`
--

INSERT INTO `kategori_vaksin` (`id_vaksin`, `nama_vaksin`, `jeda_vaksin`, `dosis`) VALUES
(1, 'Sinovac', '28 hari', 0.5),
(2, 'Astra Zeneca', '84 hari', 0.5),
(3, 'Sinopharm', '21 hari', 0.5),
(4, 'Moderna', '28 hari', 0.5),
(5, 'PFizer', '28 hari', 0.5),
(6, 'Sputnik-V', '21 hari', 0.3),
(7, 'Janssen', '21 hari', 0.5),
(8, 'Zifivax', '30 hari', 0.5);

-- --------------------------------------------------------

--
-- Table structure for table `penduduk`
--

CREATE TABLE `penduduk` (
  `id_penduduk` int(11) NOT NULL,
  `nik` int(16) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` int(16) NOT NULL,
  `akun_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penduduk`
--

INSERT INTO `penduduk` (`id_penduduk`, `nik`, `nama_lengkap`, `alamat`, `email`, `no_hp`, `akun_id`) VALUES
(1, 2147483647, 'Cynthia Deborah Nababan', 'Dusun IX Gang Horas', 'cynthiadn12@gmail.com', 2147483647, 1),
(2, 2147483644, 'Apria Theofani Nainggolan', 'Pematang Siantar', 'apriatn@gmail.com', 2147483647, 2),
(3, 2147483647, 'Johannes Bastian Jasa Sipayung', 'Perumahan Sempurna Garden', 'johannesssipayung27@gmail.com', 2147483647, 3),
(4, 2147483647, 'Jeremia Simanjuntak', 'Bekasi', 'JerSim@gmail.com', 2147483647, 4);

-- --------------------------------------------------------

--
-- Table structure for table `status_vaksinasi`
--

CREATE TABLE `status_vaksinasi` (
  `id_status` int(11) NOT NULL,
  `tanggal_vaksin` datetime NOT NULL,
  `lokasi_vaksin` varchar(100) NOT NULL,
  `vaksin_id` int(11) NOT NULL,
  `penduduk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status_vaksinasi`
--

INSERT INTO `status_vaksinasi` (`id_status`, `tanggal_vaksin`, `lokasi_vaksin`, `vaksin_id`, `penduduk_id`) VALUES
(1, '2023-02-22 10:43:25', 'Puskesmas Kuala Bali,Lubuk Pakam', 1, 1),
(4, '2023-02-22 10:46:27', 'Puskesmas Kuala Bali,Lubuk Pakam', 1, 1),
(5, '2023-02-22 10:47:27', 'Puskesmas Balige, Toba', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tempat_umum`
--

CREATE TABLE `tempat_umum` (
  `id_lokasi` int(11) NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `alamat_lokasi` varchar(100) NOT NULL,
  `kategori_lokasi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tempat_umum`
--

INSERT INTO `tempat_umum` (`id_lokasi`, `nama_lokasi`, `alamat_lokasi`, `kategori_lokasi`) VALUES
(1, 'Bukit Tara Bunga', 'Desa Lintong Nihuta, Kecamatan Tampahan,Kabupaten Tobasa', 'Objek Wisata'),
(2, 'Pantai Lumban Bul-bul', 'Desa Lumban Bulbul, Kecamatan Balige,Kabupaten Tobasa', 'Objek Wisata'),
(3, 'Air Terjun Simanindo', 'Desa Ambar Halim,Kecamatan Pintu Pohan Merant,Kabupaten TOBASA', 'Objek Wisata'),
(4, 'Institut Teknologi Del', 'Jl. P.I. Del, Sitoluama,Lagu Boti,Toba,Sumatera Utara', 'Institusi Pendidikan'),
(5, 'Labersa Toba Hotel & Convention Centre', 'Saribu Raja Janji Maria, Balige, Toba, Sumatera Utara', 'Hotel');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `check_in`
--
ALTER TABLE `check_in`
  ADD PRIMARY KEY (`id_checkIn`),
  ADD KEY `pengunjung_id` (`pengunjung_id`),
  ADD KEY `lokasi_id` (`lokasi_id`);

--
-- Indexes for table `faskes`
--
ALTER TABLE `faskes`
  ADD PRIMARY KEY (`id_faskes`);

--
-- Indexes for table `faskes_toba`
--
ALTER TABLE `faskes_toba`
  ADD PRIMARY KEY (`id_faskes`);

--
-- Indexes for table `kategori_vaksin`
--
ALTER TABLE `kategori_vaksin`
  ADD PRIMARY KEY (`id_vaksin`);

--
-- Indexes for table `penduduk`
--
ALTER TABLE `penduduk`
  ADD PRIMARY KEY (`id_penduduk`),
  ADD KEY `akun_id` (`akun_id`);

--
-- Indexes for table `status_vaksinasi`
--
ALTER TABLE `status_vaksinasi`
  ADD PRIMARY KEY (`id_status`),
  ADD KEY `vaksin_id` (`vaksin_id`),
  ADD KEY `penduduk_id` (`penduduk_id`);

--
-- Indexes for table `tempat_umum`
--
ALTER TABLE `tempat_umum`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akun`
--
ALTER TABLE `akun`
  MODIFY `id_akun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `check_in`
--
ALTER TABLE `check_in`
  MODIFY `id_checkIn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faskes`
--
ALTER TABLE `faskes`
  MODIFY `id_faskes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faskes_toba`
--
ALTER TABLE `faskes_toba`
  MODIFY `id_faskes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori_vaksin`
--
ALTER TABLE `kategori_vaksin`
  MODIFY `id_vaksin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `penduduk`
--
ALTER TABLE `penduduk`
  MODIFY `id_penduduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `status_vaksinasi`
--
ALTER TABLE `status_vaksinasi`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tempat_umum`
--
ALTER TABLE `tempat_umum`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `check_in`
--
ALTER TABLE `check_in`
  ADD CONSTRAINT `check_in_ibfk_1` FOREIGN KEY (`pengunjung_id`) REFERENCES `penduduk` (`id_penduduk`),
  ADD CONSTRAINT `check_in_ibfk_2` FOREIGN KEY (`lokasi_id`) REFERENCES `tempat_umum` (`id_lokasi`);

--
-- Constraints for table `penduduk`
--
ALTER TABLE `penduduk`
  ADD CONSTRAINT `penduduk_ibfk_1` FOREIGN KEY (`akun_id`) REFERENCES `akun` (`id_akun`);

--
-- Constraints for table `status_vaksinasi`
--
ALTER TABLE `status_vaksinasi`
  ADD CONSTRAINT `status_vaksinasi_ibfk_1` FOREIGN KEY (`vaksin_id`) REFERENCES `kategori_vaksin` (`id_vaksin`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
