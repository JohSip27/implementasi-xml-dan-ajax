<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
  session_start();
  if(!isset($_SESSION['username'])) {
    header("location:login.php");
    exit;
  }

  require 'config.php';
  ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Faskes Toba</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-info">
  <nav class="navbar navbar-expand-lg bg-secondary">
  <div class="container-fluid">
    <a href="home.php"><img src="Logo1.jpg" alt="" width="100px;" height="60px;"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-light" aria-current="page" href="faskes.php">Faskes Toba</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="checkin_als.php">Check In</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="vaksin.php">Vaksin</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="tentang.php">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="logout.php">Keluar</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="container mt-5">
    <div class="card">
        <div class="card-body">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="text-decoration-none">Home</a></li>
                <li class="breadcrumb-item">Faskes Toba</li>
            </ol>
        </nav>
              <h4>Upload file XML</h5>
            <form action="" method="post" enctype = "multipart/form-data">
              <strong>Select FILE</strong><br>
              <input type="file" name="xmlfile" class="btn"><br>
              <button class="btn btn-primary m-2">Upload File</button>
            </form>
            <?php
              $rowaffected = 0;
              if(isset($_FILES['xmlfile']) && ($_FILES['xmlfile']['error'] == UPLOAD_ERR_OK)) {
                $xml = simplexml_load_file($_FILES['xmlfile']['tmp_name']);
                foreach($xml->data as $value) {
                  $id_faskes = $value['id_faskes'];
                  $nama_faskes = $value['nama_faskes'];
                  $alamat_faskes = $value['alamat_faskes'];
                  $kategori_faskes = $value['kategori_faskes'];

                  $sql = "INSERT INTO `faskes`(`id_faskes`, `nama_faskes`, `alamat_faskes`, `kategori_faskes`) VALUES ('".$id_faskes."','.$nama_faskes.','".$alamat_faskes."','".$kategori_faskes."')";

                  $result = mysqli_query($conn, $sql);
                  if(!empty($result)) {
                    $rowaffected++;
                  } else {
                    $errorMsg = mysqli_error()."\n";
                  }
                  if($rowaffected>0) {
                    $msg = $rowaffected. "Recorded Inserted";
                  } else {
                    $errorMsg = "Failed To Insert";
                  }
                }
              }
            ?>

              <div class="alert alert-success">
                <?php 
                  if(isset($msg)) {
                    echo $msg;
                  }
                ?>
              </div>

              <div class="alert alert-success">
                <?php 
                  if(!empty($errorMsg)) {
                    echo $errorMsg;
                  }
                ?>
              </div>

            <table class="table bg-dark text-light">               
                 <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Faskes</th>
                    <th scope="col">Lokasi Faskes</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                
                $data = mysqli_query($conn,"SELECT * FROM faskes");
                while($d = mysqli_fetch_array($data))
                {?>
                    <tr>
                    <th scope="row"><?php echo $d['id_faskes'] ?></th>
                    <td><?php echo $d['nama_faskes'] ?></td>
                    <td><?php echo $d['alamat_faskes'] ?></td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
        </div>
    </div>
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>