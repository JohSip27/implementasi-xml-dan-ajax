<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
  session_start();
  if(!isset($_SESSION['username'])) {
    header("location:login.php");
    exit;
  }
  $id_user = $_SESSION['akun_id'];
  require 'config.php';
  ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profil</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-info">
  <nav class="navbar navbar-expand-lg bg-secondary">
  <div class="container-fluid">
    <a href="home.php"><img src="Logo1.jpg" alt="" width="100px;" height="60px;"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-light" aria-current="page" href="faskes.php">Faskes Toba</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="checkin_als.php">Check In</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="vaksin.php">Vaksin</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="tentang.php">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="logout.php">Keluar</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="container mt-5">
    <div class="card">
        <div class="card-body">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="text-decoration-none">Home</a></li>
                <li class="breadcrumb-item">Profil</li>
            </ol>
        </nav>
           <h3>Profil Anda</h3>
           <p>Lengkapilah Data Diri Anda dengan Mengisi Form <a href="addprofil.php" class="text-decoration-none">Add Profil</a></p>
           <form action="login_process.php" method="post">
        <?php 
                $data = mysqli_query($conn,"SELECT * FROM penduduk WHERE akun_id='  $id_user'");
                while($d = mysqli_fetch_array($data))
                {?>  
               
                    <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Username</label>
                  <input type="text" class="form-control" id="exampleFormControlInput1" name="username" value="<?php echo $_SESSION['username']; ?>">
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Nama Lengkap</label>
                  <input type="text" class="form-control" id="exampleFormControlInput1" name="namalengkap" placeholder="-" value="<?php echo $d['nama_lengkap']; ?>">
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Email</label>
                  <input type="text" class="form-control" id="exampleFormControlInput1" name="email" placeholder="-" value="<?php echo $d['email']; ?>">
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">No. KTP</label>
                  <input type="text" class="form-control" id="exampleFormControlInput1" name="noktp" placeholder="-" value="<?php echo $d['nik']; ?>">
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">Alamat</label>
                  <input type="text" class="form-control" id="exampleFormControlInput1" name="alamat" placeholder="-" value="<?php echo $d['alamat']; ?>">
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">No.HP</label>
                  <input type="number" class="form-control" id="exampleFormControlInput1" name="nohp" placeholder="-" value="<?php echo $d['no_hp']; ?>">
                  
                </div>
                </form>
                <?php }?>
        </div>
        
    </div>
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>