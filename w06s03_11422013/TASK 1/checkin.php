<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
  session_start();
  
  if(!isset($_SESSION['username'])) {
    header("location:login.php");
    exit;
  }
  
  require 'config.php';
  ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Check In</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-info">
  <nav class="navbar navbar-expand-lg bg-secondary">
  <div class="container-fluid">
    <a href="home.php"><img src="Logo1.jpg" alt="" width="100px;" height="60px;"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-light" aria-current="page" href="faskes.php">Faskes Toba</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="checkin.php">Check In</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light" href="vaksin.php">Vaksin</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="tentang.php">Tentang</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="profil.php">Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-light"href="logout.php">Keluar</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  <div class="container mt-5">
    <div class="card">
        <div class="card-body">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="text-decoration-none">Home</a></li>
                <li class="breadcrumb-item">Check In</li>
                
            </ol>
        </nav>
        <h3>Check In Terakhir Anda</h3>
        <p>Silahkan Check Out <?php echo $_SESSION['username'];?> ketika sudah keluar dari daerah yang dimasuki</p>
        <table class="table mt-5 bg-dark text-light">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Lokasi Perjalanan</th>
      <th scope="col">Tanggal/Waktu Check In</th>
      <th scope="col">Tanggal/Waktu Check Out</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
    <?php
  $batas = 5;
  $halaman = @$_GET['halaman'];
  if(empty($halaman)) {
    $posisi = 0;
    $halaman = 1;
  } else {
    $posisi = ($halaman-1) * $batas;
  }
    $id_user = $_SESSION['akun_id'];

//echo $id_user;

//$data = mysqli_query($conn,"SELECT * FROM check_in WHERE pengunjung_id = '$id_user'");
$data = mysqli_query($conn,"SELECT * FROM ((check_in INNER JOIN tempat_umum ON check_in.lokasi_id = tempat_umum.id_lokasi)INNER JOIN penduduk ON penduduk.akun_id = check_in.pengunjung_id)where pengunjung_id = '$id_user' ORDER BY id_checkIn DESC LIMIT 1");
$nomor = 1 + $posisi;
foreach($data as $d)

{?>
<tr>    
  <th scope="row"><?php echo $nomor++;?></th>
  <td><?php echo $d['nama_lokasi'];?></td>
  <td><?php echo $d['datetime_checkIn'];?></td>
  <td>  
                 <a href="checkout_process.php?id=<?= $d["id_checkIn"]; ?>" class="btn btn-success">Check Out Sekarang</a>
            </td>
  <td></td>
</tr>
<?php }?>
  </tbody>
  
</table>
<nav aria-label="Page navigation example container">

</nav>
        </div>
    </div>
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    
  </body>
</html>
