<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
  session_start();
  if(!isset($_SESSION['username'])) {
    header("location:login.php");
    exit;
  }

  require 'config.php';
  ?>
<!doctype html>
<html lang="en">
  <head>
  <script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Check In</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
  </head>
  <body class="bg-info">
  <div class="container mt-5">
    <div class="card">
        <div class="card-body">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="home.php" class="text-decoration-none">Home</a></li>
                <li class="breadcrumb-item"><a href="checkin.php" class="text-decoration-none">Riwayat Check In</a></li>
                <li class="breadcrumb-item">Check In</li>
                
            </ol>
        </nav>
        <h3>Form Check In</h3>
        <form action="addcheckin_process.php" method="post">
            <p>Pantau Perjalanan Anda dengan mengisi form di bawah ini</p>
            <label for="lokasi">Lokasi</label>
                      <select class="form-select" aria-label="Default select example" name="lokasi" >
                        <option selected></option>
                        <option value="4">Institut Teknologi Del</option>
                        <option value="1">Bukit Tara Bunga</option>
                        <option value="2">Pantai Lumban Bul-bul</option>
                        <option value="3">Air Terjun Simanindo</option>
                        <option value="5">Labersa Toba Hotel & Convention Centre</option>                        
                    </select>
            <button class="btn btn-success mt-4">Check In</button>
        </form>
        </div>
    </div>
  </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    <script src="jsajax.js"></script>
  </body>
</html>