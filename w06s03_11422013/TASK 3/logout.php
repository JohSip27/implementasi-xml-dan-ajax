<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
    session_start();
    $_SESSION = [];
    session_unset();
    session_destroy();

    header("location:login.php");
    exit;
    

?>